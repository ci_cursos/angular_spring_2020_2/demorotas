import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaginanaoencontradaComponent } from './paginanaoencontrada/paginanaoencontrada.component';

const routes: Routes = [
  { path: '**', component: PaginanaoencontradaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
