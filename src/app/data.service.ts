import { Injectable } from '@angular/core';
import { InMemoryDbService, RequestInfo } from 'angular-in-memory-web-api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService extends InMemoryDbService {

  createDb() {
    const produtosws = [
      { id: 1, nome: 'Produto 1', preco: 1.99 },
      { id: 2, nome: 'Produto 2', preco: 1.99 },
      { id: 3, nome: 'Produto 3', preco: 13.99 },
    ];
    return {produtosws};
  }

}
