import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProdutosRoutingModule } from './produtos-routing.module';
import { ListaprodutosComponent } from './listaprodutos/listaprodutos.component';
import { DetalhesprodutoComponent } from './detalhesproduto/detalhesproduto.component';


@NgModule({
  declarations: [ListaprodutosComponent, DetalhesprodutoComponent],
  imports: [
    CommonModule,
    ProdutosRoutingModule
  ]
})
export class ProdutosModule { }
