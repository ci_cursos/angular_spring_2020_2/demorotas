import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Produto } from '../produto';
import { ProdutosService } from '../produtos.service';

@Component({
  selector: 'app-listaprodutos',
  templateUrl: './listaprodutos.component.html',
  styleUrls: ['./listaprodutos.component.css']
})
export class ListaprodutosComponent implements OnInit {
  produtos: Observable<Produto[]> = of<Produto[]>();
  mensagemErro: string = '';

  constructor(private produtosService: ProdutosService) { }

  ngOnInit(): void {
    this.produtos = this.produtosService.getProdutos().pipe(
      catchError(erro => {
        this.mensagemErro = erro;
        return of<Produto[]>();
      })
    );
  }

}
