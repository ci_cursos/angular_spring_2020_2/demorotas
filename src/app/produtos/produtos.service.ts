import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Produto } from './produto';
import { Observable, of, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProdutosService {
  private api = environment.api;

  constructor(private http: HttpClient) { }

  public getProdutosMockup(): Observable<Produto[]> {
    const produtos: Produto[] = [
      {id:1, nome:"teste", preco:1.99},
      {id:2, nome:"outro", preco:1.99}
    ];
    return of(produtos);
  }

  public getProdutos(): Observable<Produto[]> {
    return this.http.get<Produto[]>(`${this.api}/produtosws`).pipe(
      tap(dados => console.log('get produtos')),
      catchError(this.handleError)
    );
  }

  public getProduto(id: number): Observable<Produto> {
    return this.http.get<Produto>(`${this.api}/produtosws/${id}`);
  }

  public handleError() {
    return (erro: HttpErrorResponse) => {
      let msg = '';
      if (erro.error instanceof ErrorEvent) {
        //falha no clado cliente
        msg = `Falha no cliente: ${erro.error.message}`;
      } else {
        //falha no lado servidor
        msg = `Falha no servidor: ${erro.status}, ${erro.statusText}, ${erro.message}`;
      }
      return throwError(msg);
    }
  }
}
