import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProdutosService } from '../produtos.service';

@Component({
  selector: 'app-detalhesproduto',
  templateUrl: './detalhesproduto.component.html',
  styleUrls: ['./detalhesproduto.component.css']
})
export class DetalhesprodutoComponent implements OnInit {
  idProduto: number = -1;
  constructor(
    private route: ActivatedRoute,
    private produtosService: ProdutosService
  ) { }

  ngOnInit(): void {
    this.idProduto = this.route.snapshot.params.id;
  }

}
