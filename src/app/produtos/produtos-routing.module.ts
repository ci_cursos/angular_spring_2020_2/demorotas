import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaprodutosComponent } from './listaprodutos/listaprodutos.component';
import { DetalhesprodutoComponent } from './detalhesproduto/detalhesproduto.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'produtos',
        children: [
          {
            path: '',
            component: ListaprodutosComponent
          },
          {
            path: ':id',
            component: DetalhesprodutoComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProdutosRoutingModule { }
